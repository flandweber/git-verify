{-# Language FlexibleContexts #-}

module OpenSshParser where

import Text.Parsec
import Data.Bits as Bits
import Text.Parsec.String --TODO: change to ByteString (as this is the input format)
import qualified Data.Binary as Binary
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as Char8
import Data.Word
import qualified Data.Text as T
import qualified Data.Text.Encoding.Base64 as B64
import qualified Crypto.PubKey.Ed25519 as Ed25519
import qualified Crypto.PubKey.RSA as RSA
import qualified Crypto.Error

data PublicKey = Ed25519K Ed25519.PublicKey | RsaK RSA.PublicKey deriving (Eq, Show)

data Signature = Ed25519S Ed25519.Signature | RsaS BS.ByteString deriving (Eq, Show)

data SshSignature = SshSignature
  { publickey :: PublicKey
  , namespace :: String
  , hash_algorithm :: String
  , signature :: Signature 
  } deriving (Eq, Show)

parseSshSignature = parse sshSignatureParser "signature"

recurseParse p i = do
  i' <- getInput
  setInput i
  r <- p 
  setInput i'
  return r

-- https://www.rfc-editor.org/rfc/rfc4251#section-5
uint32Parser :: Parser Word32
uint32Parser = do 
  s::String <- count 4 anyChar
  return $ Binary.decode $ BS.fromStrict $ Char8.pack s


stringParser :: Parser String
stringParser = do
  l <- uint32Parser
  count (fromIntegral l) anyChar

byteStringParser :: Parser BS.ByteString
byteStringParser = Char8.pack <$> stringParser

mpintParser :: Parser Integer
mpintParser = fromByteString <$> byteStringParser
  where
    fromByteString bs = case BS.unpack bs of
      [] -> 0
      h:t -> let 
          negative = testBit h 7
          fromWord8s bs = sum $ zipWith (\(n::Integer) (w::Word8) -> (256 ^ n) * (toInteger w)) [0, 1 ..] $ reverse bs
          --unsignedN = Binary.decode $ LBS.pack $ (clearBit h 7):t 
          unsignedN = fromWord8s $ (clearBit h 7):t 
          len = if h == 0 then length t else length (h:t)
        in 
          if negative then -1 * unsignedN else unsignedN
    
publicKeyParser :: Parser PublicKey
publicKeyParser = do
  format <- stringParser
  case format of
    "ssh-ed25519" -> do
      bits <- byteStringParser
      return $ Ed25519K $ Crypto.Error.throwCryptoError $ Ed25519.publicKey bits
    "ssh-rsa" -> do
      n <- mpintParser
      e <- mpintParser
      return $ RsaK $ RSA.PublicKey 3072 n e --TODO: is it always 3072?
    f -> unexpected $ "key format " <> f

signatureParser :: Parser Signature
signatureParser = do
  format <- stringParser
  case format of
    "ssh-ed25519" -> Ed25519S . Crypto.Error.throwCryptoError . Ed25519.signature <$> byteStringParser
    "rsa-sha2-512" -> RsaS <$> byteStringParser 
    f -> unexpected $ "signature format " <> f

--https://github.com/openssh/openssh-portable/blob/8241b9c0529228b4b86d88b1a6076fb9f97e4a99/PROTOCOL.sshsig#L34
unarmoredSshSignatureParser :: Parser SshSignature
unarmoredSshSignatureParser = do 
    _ <- string "SSHSIG"
    signatureVersion <- uint32Parser
    if signatureVersion /= 1 then unexpected "Signature version unsupported"
    else 
      do
        publickey <- recurseParse publicKeyParser =<< stringParser
        namespace <- stringParser
        _ <- stringParser
        hash_algorithm <- stringParser
        signature <- recurseParse signatureParser =<< stringParser
        return $ SshSignature publickey namespace hash_algorithm signature

sshSignatureParser :: Parser SshSignature
sshSignatureParser = header *> blob <* footer
  where 
    header = string "-----BEGIN SSH SIGNATURE-----"
    footer = string "-----END SSH SIGNATURE-----"
    blob = do 
      b64 <- sepBy1 (many $ noneOf ['\n', '-']) (char '\n') -- TODO: simplify
      case B64.decodeBase64 $ T.pack $ concat b64 of
        Left e -> unexpected $ T.unpack e
        Right r -> recurseParse unarmoredSshSignatureParser $ T.unpack r

