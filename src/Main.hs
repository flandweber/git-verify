{-# Language BlockArguments #-}
{-# Language FlexibleContexts #-}

module Main where
import Filesystem.Path.CurrentOS (decodeString)
import System.Exit
import System.Process
import qualified Data.List as List
import qualified Data.Map.Strict as M
import Data.Map.Strict ((!?))
import Data.String
import Data.Git hiding (withRepo, getCommit)
import Data.Git.Monad
import Data.Git.Ref
import Data.Git.Diff
import Data.Git.Storage.Object
import qualified Data.ByteString.Char8 as BS
import qualified Data.Aeson as Json
import Options.Applicative
import Control.Monad

import Check
import Committer
import OpenSshParser

type CommitSignature = String
type SignedData = String
type Reason = String
-- TODO: maybe replace ValidationResult and VerificationResult with Exception Monad or similar constucts?
data ValidationResult = ValidationSuccess | ValidationFailure Reason deriving Show -- States if given parent really verifies the given commit
data VerificationResult = VerificationSuccess | VerificationFailure (M.Map String (Either ValidationResult VerificationResult)) deriving Show --TODO: M.Map String (Either ... should be changed to M.Map Commit r ... since String represents CommitOid

bsdToPath bsd = case bsd of
                     OnlyOld (BlobState fn _ _ _) -> fn
                     OnlyNew (BlobState fn _ _ _) -> fn
                     OldAndNew _ (BlobState fn _ _ _) -> fn

bsToEp :: BS.ByteString -> EntPath
bsToEp = map entName . BS.splitWith (=='/')

fpToEp :: FilePath -> EntPath
fpToEp = bsToEp . BS.pack

prefixMatch :: EntPath -> EntPath -> Bool
prefixMatch [] [] = True -- File matches
prefixMatch (x:xs) (y:ys) = x == entName BS.empty || x == y && prefixMatch xs ys
prefixMatch _ _ = False

anyPrefixMatch prefixes path = any (`prefixMatch` path) prefixes

getProtected protected unprotected = case unprotected of 
  Just unprotected' -> filter $ not . anyPrefixMatch (map fpToEp unprotected')
  Nothing -> case protected of
    Just protected' -> filter $ anyPrefixMatch $ map fpToEp protected'
    Nothing -> id

validate committersFilePath signature (signedData::BS.ByteString) commitRef parentRef = do
  git <- getGit
  maybeCommittersFile <- withCommit parentRef $ getFile $ bsToEp committersFilePath
  case maybeCommittersFile of
    Nothing -> return $ ValidationFailure "committers file not found"
    Just bs -> case Json.decode bs of
      Nothing -> return $ ValidationFailure "committers file invalid"
      Just (CommittersFile protected unprotected committers _) -> do
        changed::[EntPath] <- liftGit $ getDiffWith (\bsd acc -> bsdToPath bsd:acc) [] parentRef commitRef git
        let protectedChanged = getProtected protected unprotected changed
        if null protectedChanged
        then return ValidationSuccess
        else do
          Person committerName committerEmail _ <- withCommit commitRef getCommitter
          case committers !? BS.unpack committerName of 
            Nothing -> return $ ValidationFailure "committer could not be found in the committers file"
            Just (CommitterRecord email publicKey allowed)
              | email /= BS.unpack committerEmail -> return $ ValidationFailure "committer email does not match the one in the committers file"
              | not $ checkSignature signature signedData publicKey -> return $ ValidationFailure "cryptographic verification did not succeed."
              | otherwise -> case allowed of 
                    Nothing -> return ValidationSuccess
                    Just allowed' -> do
                      let prefixesMatch prefixes = all (anyPrefixMatch prefixes)
                      if map fpToEp allowed' `prefixesMatch` protectedChanged
                      then return ValidationSuccess
                      else return $ ValidationFailure $ "Committer " <> BS.unpack committerName <> "is not authorized for this change."

verified :: GitVerifyOptions -> Ref SHA1 -> Ref SHA1 -> GitM VerificationResult
verified options@(GitVerifyOptions _ _ _ committersFilePath q) introRef commitRef
  | introRef == commitRef = return VerificationSuccess
  | otherwise = do
    Just commit <- getCommit commitRef
    let parents = commitParents commit
    unless q $ liftGit $ print $ "verifying " <> show commitRef
    autoMerged <- if length parents <= 1
      then return False
      else do
        (exitCode, stdout, _) <- liftGit $ readProcessWithExitCode "git" (["merge-tree"] <> map show parents) ""
        return $ case exitCode of
          ExitFailure _ -> False
          ExitSuccess -> init stdout == show ( commitTreeish commit)
    valids::[ValidationResult] <- if autoMerged 
      then return $ repeat ValidationSuccess -- if commit was autoMerged then all parents are valid
      else do 
        case List.partition (\(CommitExtra n _) -> n == "gpgsig") $ commitExtras commit of
          ([CommitExtra "gpgsig" signature], extraHeaders) -> do 
            let signedData::BS.ByteString = BS.toStrict $ getRaw $ Commit (commitTreeish commit) parents (commitAuthor commit) (commitCommitter commit) (commitEncoding commit) extraHeaders (commitMessage commit)
            let Right parsedSignature = parseSshSignature (BS.unpack signature)
            mapM (validate committersFilePath parsedSignature signedData commitRef) parents
          _ -> return $ repeat $ ValidationFailure "Commit not signed."
    verifieds <- mapM (verified options introRef) parents
    let results = zipWith parentResult valids verifieds
    let oidsParents = map show parents
    return $ if all fst results then VerificationSuccess else VerificationFailure $ M.fromList $ zip oidsParents $ map snd results
    where
      parentResult valid verified =
         case valid of
          ValidationFailure _ -> (False, Left valid)
          _ -> case verified of
            VerificationFailure _ -> (False, Right verified)
            _ -> (True, undefined)

gitVerify :: GitVerifyOptions -> IO ()
gitVerify options@(GitVerifyOptions i r target _ q) = do
  Right result <- withRepo (decodeString r) do
    git <- getGit
    targetRef::Ref SHA1 <- case target of
      Reference ref -> do
        mTargetRef <- liftGit $ resolveRevision git $ fromString ref
        case mTargetRef of
          Nothing -> error $ "Couldn't find reference " ++ ref
          Just r -> return r
      Revision rev -> return $ fromHexString rev
    let introRef = fromHexString i
    verified options introRef targetRef
  if q then return () else print result;
  case result of
    VerificationSuccess -> exitSuccess
    _ -> exitWith (ExitFailure 1)

data Target = Reference String | Revision String deriving Show --TODO: maybe not needed?
targetOption :: Parser Target
targetOption =
  Reference <$> strOption
     ( long "ref"
    <> metavar "REFERENCE"
    <> value "HEAD"
    <> help "Tag or Branch that points to the commit to verify" )
  <|> 
  Revision <$> strOption
     ( long "rev"
    <> metavar "COMMIT"
    <> help "Commit to verify" )


data GitVerifyOptions = GitVerifyOptions
  { intro :: String
  , repo :: String
  , target :: Target
  , committersFilePath :: BS.ByteString
  , quiet :: Bool
  } deriving Show
options :: Parser GitVerifyOptions
options = GitVerifyOptions
  <$> strOption
      ( long "intro"
     <> short 'i'
     <> metavar "COMMIT"
     <> help "Commit to start the verification from" )
  <*> strOption
      ( long "repo"
     <> short 'r'
     <> metavar "PATH"
     <> value ".git"
     <> help "Repository to verify" )
  <*> targetOption
  <*> strOption
      ( long "committers-file"
     <> metavar "PATH"
     <> value "committers.json"
     <> help "File in the repoitory file tree to start the verification from (relative to root of the repository)" )
  <*> switch
      ( long "quiet"
     <> short 'q'
     <> help "Mute result output" )

main :: IO ()
main = execParser opts >>= gitVerify
        where
                opts = info (options <**> helper) $ fullDesc
                        <> header "git-verify - in-band commit verification"
                        <> progDesc "Verify all Commits with their parents committers.json"
