{-# Language DeriveGeneric #-}

module Committer where
import qualified Data.Text as T
import GHC.Generics
import Data.Aeson
import qualified Data.Aeson.Types
import qualified Data.Map as M
import Text.Parsec
import qualified Data.Text.Encoding.Base64 as B64

import OpenSshParser

type CommitterName = String

data CommittersFile = CommittersFile 
  { protected :: Maybe [FilePath]
  , unprotected :: Maybe [FilePath]
  , committers :: M.Map CommitterName CommitterRecord
  , automerges :: Maybe Bool
  } deriving (Generic, Show)
instance FromJSON CommittersFile

data CommitterRecord = CommitterRecord 
  { email :: String
  , publicKey :: PublicKey
  , allowed :: Maybe [FilePath]
  } deriving (Generic, Show)
instance FromJSON CommitterRecord

instance FromJSON PublicKey where 
  parseJSON = withText "PublicKey" $ \s -> case B64.decodeBase64 s of 
    Right s' -> case parse publicKeyParser "publicKey" (T.unpack s') of
      Left _ -> Data.Aeson.Types.unexpected $ String s' -- TODO: improve error message
      Right key -> return key

