module Check where
import qualified Data.Binary as Binary
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSLazy
import qualified Data.ByteString.Base16 as Base16
import qualified Data.ByteString.Char8 as Char8
import qualified Crypto.PubKey.Ed25519 as Ed25519
import qualified Crypto.PubKey.RSA.PKCS15 as RSA
import OpenSshParser
import Crypto.Hash
import Data.Word

--checkSignature signature signedData publickey = do
--  let authorized_signers = "dummy-principal ssh-ed25519 " <>publickey <> " -"
--  withTempDirectory "." "verification" $ \verificationDir -> do
--    signatureFile <- writeTempFile verificationDir "signature" signature
--    authorizedSignersFile <- writeTempFile verificationDir "authorized_signers" authorized_signers
--    (e, stdout, stderr) <- readProcessWithExitCode "ssh-keygen" ["-Y", "verify", "-n", "git", "-s", signatureFile, "-f", authorizedSignersFile, "-I", "dummy-principal"] signedData
--    let output = stderr <> stdout
--    return $ case e of
--      ExitSuccess -> (True, output)
--      ExitFailure n -> (False, output)

verify :: PublicKey -> BS.ByteString -> Signature -> Bool
verify (Ed25519K publickey) signedData (Ed25519S signature) = Ed25519.verify publickey signedData signature
verify (RsaK publickey) signedData (RsaS signature) = RSA.verify (Nothing::Maybe SHA512) publickey signedData signature

checkSignature :: SshSignature -> BS.ByteString -> PublicKey -> Bool
checkSignature (SshSignature _ namespace hash_algorithm signature) message publickey = 
  verify publickey signedData signature
  where
    toOpenSshString :: BS.ByteString -> BS.ByteString
    toOpenSshString s = (BSLazy.toStrict $ Binary.encode l) <> s
      where l :: Word32 = fromIntegral $ BS.length s
    algorithmFromString "sha256" = show . hashWith SHA256
    algorithmFromString "sha512" = show . hashWith SHA512
    signedData = let
        Right hash = Base16.decode $ Char8.pack $ algorithmFromString hash_algorithm message
      in 
        "SSHSIG" <> (BS.concat $ map toOpenSshString [Char8.pack namespace, "" {-reserved-}, Char8.pack hash_algorithm, hash])

