# `git-verify` - in-band commit verification

`git-verify` builds on [git commit signatures](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work).
It aims to make source code verification easy and automatable.

As of yet the program is limited to ed25519 ssh signatures. 
RSA ssh signatures are planned but [don't work yet](https://codeberg.org/flandweber/git-verify/issues/2).

`git-verify` was heavily inspired by the [guix authentication scheme](https://guix.gnu.org/en/blog/2020/securing-updates), but has more [features](#features) than the `guix git authenticate` command to make it applicable to a wider range of scenarios.

Please [notify me](mailto:finn@landweber.xyz) if you use the project with an open source repo, so I can take care not to accidentally break your use case.

## How it helps

`git-verify` can help detect malicious updates from compromised remote repositories.
Those could be a compromise of the webforge (i. e. GitHub or a self-hosted git server) as well as compromised committer accounts.
It also allows repository owners to give [granular access](#granular-authorization) to their committers and thous minimize the potential damage of a compromise.

## How to verify a repository

For a verification of commits you always need an 'introduction', from which on all commits should be verified.
An introduction is usually the first commit with a `committers.json`.
Once you have the sha1 hash of said commit you can run `git-verify --intro <hash>` to verify the current HEAD.
If the hash of the introduction was obtained in a secure way (or via "trust on first use" before the repository was compromised), then `git-verify` protects against commits without authorized keys.

On this repository the first commit serves as an introduction.
To verify the current commit of this repository you can run 
```sh
git clone https://codeberg.org/flandweber/git-verify.git
cd git-verify
cabal run -- git-verify --intro 1c7af1ded2e269d4badaf3bb0aff7d52e6519d97
```
although you usually shoulnd't use the 'untrusted' version of this program to run the verification.

## How to create a verifiable repository

To use it in your project, you'll need to create a `committers.json` with the allowed committers of the project (see this repo for an example file) and commit it to the repository.
This commit will be the introduction, the root of trust for your verification.
If you want to change the committers keys, authorizations and protected files you simply commit the changes and they will be used for the verification of all following commits.
The committers data structure, that is used to derive the parser, can be found at [src/Committer.hs](src/Committer.hs#L17).

## Features

### Protected and unprotected files

The `committers.json` format allows you to specify `unprotected` files which should be disregarded in the checks.
Use a trailing `/` to specify folders.
If for example, if the path `doc/` is unprotected, all commits changing only files in this folder will be considered valid.

If `protected` is specified instead, only commits changing the corresponding files will be scrutinized.

If both are specified, `protected` is ignored.

### Granular Authorization

An `allowed` attribute may be specified for each committer, e. g.
```json
{"email": "...", "publicKey": "...", "allowed": [ "doc/", "committers.json" ] }
```
The committers signature is then only valid for paths in `allowed`.

### Unsigned Auto-Merges

Merge commits don't need to be signed if they can be reproduced by merging their signed parents.
This slightly worsens `git-verify`s security properties, but allows continuing to use the merge button of web-forges.

Currently unsigned auto-merges are enabled for all verification, but it might become an opt-in feature later to allow projects that don't rely on it to retain stricter security properties.
This is why the `committers.json` format already allows specification of an top level boolean `automerges`, that is currently ignored.

## Nix Integration

The Project is supposed to help make [Nix](https://nixos.org) updates more secure.

In [`fetchgitverify/package.nix`](fetchgitverify/fetcher.nix) you can find a simple git fetcher that runs `git-verify` on the fetched code.
It is based on the [nixpkgs git fetcher](https://github.com/NixOS/nixpkgs/tree/master/pkgs/build-support/fetchgit) and can take the same arguments, so that it should feel familiar to the most Nix users:

```nix
fetchgitverify {
    url = "https://codeberg.org/flandweber/git-verify.git"; 
    intro = "1c7af1ded2e269d4badaf3bb0aff7d52e6519d97"; 
    rev = "3d1432359d0d5678ba4f9f68afc85164da51250d";
    hash = "sha256-015Izoa8Tf5Lohtp6foEjhl739ZZ3hCfMmXsuDbEdMI=";
}
```

## Usual Word of Warning

This project is - as of now - a proof of concept and under development.
Please don't rely on it as your only protection.

I'd appreciate any reviews of the security of `git-verify`.

I'm aware that hosting on [codeberg.org](https://docs.codeberg.org/getting-started/what-is-codeberg) poses an additional hurdle for participation.
If that's easier for you, please feel free to sent bugs and patches [via email](mailto:finn@landweber.xyz) instead.
