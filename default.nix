{pkgs ? import <nixpkgs> {}}:
pkgs.haskellPackages.developPackage {
  root = ./.;
  source-overrides = {
    git = builtins.fetchGit {
      url = "https://github.com/flandweber/hs-git.git";
      rev = "a15600e9c4ee5afce9324ece52fab2bd472a4fce";
    };
  };
}
