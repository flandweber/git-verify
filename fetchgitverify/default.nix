{pkgs ? import <nixpkgs> {}}: let
  git-verify = (import ./.. {inherit pkgs;}).out;
in
  pkgs.callPackage ./fetcher.nix {inherit git-verify;}
