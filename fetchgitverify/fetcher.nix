{
  git-verify,
  fetchgit,
  runCommand,
  rsync,
}: (
  {
    intro,
    rev,
    deepClone ? true,
    leaveDotGit ? false,
    ...
  } @ args:
    assert deepClone; let
      src = fetchgit ((builtins.removeAttrs args ["intro"])
        // {
          deepClone = true;
          leaveDotGit = true;
        });
    in
      runCommand src.name {buildInputs = [git-verify rsync];} ''
        git-verify --intro ${intro} --repo ${src}/.git --rev ${rev}
        rsync -r ${
          if leaveDotGit
          then ""
          else "--exclude .git"
        } ${src}/ $out 
      ''
)
