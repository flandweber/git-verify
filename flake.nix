{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
      developPackagesDerivation = import ./default.nix {inherit pkgs;};
    in {
      packages.default = developPackagesDerivation.out;
      devShells.default = developPackagesDerivation;
    });
}
